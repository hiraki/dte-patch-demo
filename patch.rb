#! /usr/bin/ruby 

require "./topology"

class Patch < Topology
 
  def initialize start=0, demo=false
    super 2, start, demo, ipOctedStart=7
  end

  def create nPorts
    initSwitches
    addPorts nPorts 
  end

  def addPorts n
    (1..n).each do |i|
      sw1 = @switches[0][:name]
      sw2 = @switches[1][:name]
      src = ovsPort 0
      dst = ovsPort 1
      createLink src, dst
      ovsPortAdd sw1, src
      ovsPortAdd sw2, dst
    end
  end

end


p = Patch.new(6, false)
p.setController("172.16.101.11", "6653")
p.create(1)
p.setIpPrefix "172.16.1."
p.addHost(0)
p.addHost(0)
p.addHost(0)
p.addHost(0)
p.addHost(1)
p.addHost(1)
p.addHost(1)
p.addHost(1)
