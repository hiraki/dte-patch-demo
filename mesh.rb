#!/usr/bin/ruby

require "./topology"

class Mesh < Topology
  
  def initialize n=0, start=0, demo=false
    super(n, start, demo)
  end

  def create
    initSwitches
    addPorts 0
  end

  def addPorts n
    puts "------------ add-port br#{n} -----------------"
    ((n+1)..(@nsw-1)).each do |i|
      srcSw = br n
      dstSw = br i
      srcPort = ovsPort n
      dstPort = ovsPort i
      createLink srcPort, dstPort
      ovsPortAdd srcSw, srcPort
      ovsPortAdd dstSw, dstPort
    end
    return if n >= @nsw - 1
    addPorts n+1
  end

end


m = Mesh.new(5, 0, false)
m.setController("172.16.101.10", "6653")
m.create()
m.addHost(0)
m.addHost(1)
m.addHost(2)
