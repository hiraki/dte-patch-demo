class Topology

  def initialize n=0, start=0, demo=false, ipOctedStart=1
    @controller = { ip: "172.16.101.10", port: "6653" }
    @ofVersion = "OpenFlow13"
    @swPrefix = "br"
    @portPrefix = "ovs"
    @hostPrefix = "ns"
    @ethPrefix = "veth"
    @IpPrefix = "192.168.1."
    @ipMask = 24
    @lastIpOctet = ipOctedStart
    @lastEth = 0
    @nsw = n
    @start = start
    @switches = []
    @ports = []
    @hosts = []
    @demo = demo
  end

  def initSwitches
    range = @start..(@nsw+@start-1)
    range.each do |i|
      s = { name: "#{@swPrefix}#{i}", lastPortNo: 1, lastHostNo: 1, number: i}
      @switches << s
    end

    addSwitches 
  end

  def addSwitches
    puts "------------ add-br -----------------"
    @switches.each_with_index do |v, i|
      execCmd "ovs-vsctl del-br #{v[:name]}"
    end
    @switches.each_with_index do |v, i|
      execCmd "ovs-vsctl add-br #{v[:name]} -- set bridge #{v[:name]} protocols=#{@ofVersion} other-config:datapath-id=#{sprintf("%016d",v[:number]+10000)}"
      execCmd "ovs-vsctl set-controller #{v[:name]} tcp:#{@controller[:ip]}:#{@controller[:port]}"
      execCmd "ovs-vsctl set-fail-mode #{v[:name]} secure"
    end
  end

  def execCmd str
    puts str
    `#{str}` if !@demo
  end

  def br i
    return "#{@swPrefix}#{i}"
  end

  def ovsPort sw
    port = "#{@portPrefix}#{@switches[sw][:number]}-#{@switches[sw][:lastPortNo]}"
    @switches[sw][:lastPortNo] += 1
    return port
  end

  def ovsPortAdd sw, port
    execCmd "ovs-vsctl add-port #{sw} #{port}"
  end

  def newVeth
    port = "#{@ethPrefix}#{@lastEth}"
    @lastEth += 1
    return port
  end

  def createLink src, dst
    execCmd "ip link del #{src}" 
    execCmd "ip link del #{dst}" 
    execCmd "ip link add name #{src} type veth peer name #{dst}"
    linkUp src
    linkUp dst
  end

  def linkUp port
    execCmd "ip link set #{port} up"
  end

  def host sw
    host = "#{@hostPrefix}#{@switches[sw][:number]}-#{@switches[sw][:lastHostNo]}"
    @switches[sw][:lastHostNo] += 1
    return host
  end

  def setIp host, veth
    execCmd "ip netns exec #{host} ip addr add #{@IpPrefix}#{@lastIpOctet}/#{@ipMask} dev #{veth}"
    @lastIpOctet += 1
  end

  def setController ip, port
    @controller = { ip: ip, port: port }
  end

  def setIpPrefix str
    @IpPrefix = str
  end

  def addHost swNum
    hostName = host swNum
    puts "------------ add host #{hostName} to #{br swNum} -----------------"
    h = { name: "#{hostName}", switch: "#{@swPrefix}#{swNum}" }
    s = @switches[swNum]
    if @hosts.include? h || !s
      return
    else
      @hosts << h
    end

    swPort = ovsPort swNum
    veth = newVeth
    createLink swPort, veth

    execCmd "ip netns del #{hostName}"
    execCmd "ip netns add #{hostName}"
    execCmd "ip netns exec #{hostName} ip link set lo up"
    execCmd "ip link set netns #{hostName} #{veth} up"
    setIp hostName, veth

    ovsPortAdd s[:name], swPort 
    
    linkUp swPort
  end

end


